import * as React from 'react'
import classnames from 'classnames'

import '@styles/layout'

type justifyContent = 'flex-start' | 'flex-end' | 'center' | 'space-between' | 'space-around'
type alignItems = 'flex-start' | 'flex-end' | 'center' | 'baseline' | 'stretch'

interface IFlexProps {
  className?: string
  justifyContent?: justifyContent
  alignItems?: alignItems
}

interface IRowProps extends IFlexProps {}
interface IColumnProps extends IFlexProps {}

const flexProps = (baseClass: string, props: React.Props<any> & IRowProps): string => (
  classnames(
    baseClass,
    props.className,
    {
      [`justify-content-${props.justifyContent}`]: props.justifyContent !== undefined,
      [`align-items-${props.alignItems}`]: props.alignItems !== undefined
    }
  )
)

export const Row = (props: React.Props<any> & IRowProps) => (
  <section className={flexProps("row", props )}>
    { props.children }
  </section>
)

export const Column = (props: React.Props<any> & IColumnProps) => (
  <section className={flexProps("column", props)}>
    { props.children }
  </section>
)
