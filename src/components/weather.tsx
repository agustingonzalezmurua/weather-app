import * as React from 'react'

import WeatherCard, { WeatherCardLower, WeatherCardUpper } from '@components/presentation/weather-card'
import Card from '@components/presentation/card'
import Button from '@components/presentation/button'
import Icon from '@components/presentation/weather-icon'
import Clock from '@components/clock'
import WeatherProvider from '@providers/weather'
import * as Layout from '@components/layout'

type WeatherInfo = {
  id: number
  main: string
  description: string
  icon: string
}

type WeatherResponse = {
  name: string
  coord: number
  main: {
    temp: number
    pressure: number
    humidity: number
    temp_min: number
    temp_max: number
  }
  sys: {
    country: string
    id: number
    sunrise: number
    sunset: number
  }
  wind: {
    speed: number
    deg: number
  }
  weather: WeatherInfo[]
}

const determineIcon = (info: WeatherInfo) => {
  let isNight = info.icon.search('n') !== -1
  return `${isNight ? 'night' : 'day'}-${info.id}`
}

interface IWeatherState {
  owm: WeatherResponse | null
  isRefreshBlocked: boolean
  permissionDenied: boolean
  isLoading: boolean
}

export default class Weather extends React.Component<any, IWeatherState> {

  state: IWeatherState = {
    owm: null,
    isRefreshBlocked: false,
    permissionDenied: false,
    isLoading: true
  }

  componentDidMount() {
    this.refreshPosition(false)
    setInterval(() => this.refreshPosition(false), 1000 * 60)
  }

  render() {
    if (!this.state.owm || this.state.isLoading) return <this.renderLoading/>
    if (this.state.permissionDenied) return <this.renderPermissionDenied/>
    return (
      <WeatherCard>
        <WeatherCardUpper>
          <Layout.Column className="fill-width fill-height" justifyContent="center" alignItems="center">
            <section className="before-icon">
              <h1>{this.state.owm.name}</h1>
            </section>
            <Icon icon={determineIcon(this.state.owm.weather[0])} />
            <Layout.Column className="after-icon" justifyContent="center" alignItems="center">
              <h3>{this.state.owm.weather[0].description.toLocaleUpperCase()}</h3>
              <Layout.Row justifyContent="center" alignItems="center">
                <section className="temperature">
                  {this.state.owm.main.temp}°
                </section>
                <Clock />
              </Layout.Row>
              <h2>Min {this.state.owm.main.temp_min}º | Max {this.state.owm.main.temp_max}º</h2>
              <h2>Humidity: {this.state.owm.main.humidity}%</h2>
            </Layout.Column>
          </Layout.Column>
        </WeatherCardUpper>

        <WeatherCardLower>
          <Layout.Column justifyContent="center">
            <Layout.Row>
            </Layout.Row>
            <Button disabled={this.state.isRefreshBlocked} onClick={() => this.refreshPosition(true)}>
              {this.state.isRefreshBlocked ? 'blocked for 1 min' : 'Refresh current position'}
            </Button>
          </Layout.Column>
        </WeatherCardLower>
      </WeatherCard>
    )
  }

  renderPermissionDenied = () => (
    <Card>
      You need to allow this site to reach your location in order for it to work
    </Card>
  )

  renderLoading = () => (
    <Card>
      Loading...
    </Card>
  )

  refreshPosition = (blocksRefresh: boolean): void => {
    this.setState({ isLoading: true })
    navigator.geolocation.getCurrentPosition(
      (position) => {
        WeatherProvider.byGeo(position)
          .then((response: WeatherResponse) => {
            localStorage.setItem('weather', JSON.stringify(response))
            this.setState({ owm: response, isRefreshBlocked: blocksRefresh, isLoading: false })
            if (blocksRefresh) // Unblocks the refresh button after 1 minute
              window.setTimeout(() => this.setState({ isRefreshBlocked: false }), 1000 * 60)
          })  
      }, (error) => {
        if (error.code === error.PERMISSION_DENIED)
          this.setState({ permissionDenied: true })
      }
    )
  }
}