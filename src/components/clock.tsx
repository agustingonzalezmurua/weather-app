import * as React from 'react'

export interface IClockState {
  date: Date
}

export default class Clock extends React.Component<any, IClockState> {

  state: IClockState = {
    date: new Date()
  }

  constructor(props: any) {
    super(props)
    this.timer = window.setInterval(this.tick, 1000)
  }

  timer: number

  componentWillUnmount() {
    window.clearTimeout(this.timer)
  }

  tick = () => this.setState({ date: new Date() })

  render() {
    return(
      <section className="clock">
        { this.state.date.toLocaleTimeString() }
      </section>
    )
  }
}

