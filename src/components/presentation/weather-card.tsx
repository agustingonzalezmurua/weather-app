import * as React from 'react'
import * as Layout from '@components/layout'
import '@styles/weather-card'

const WeatherCard = (props: React.Props<any>) => (
  <Layout.Column className="weather-card">
    { props.children }
  </Layout.Column>
)

export const WeatherCardUpper = (props: React.Props<any>) => (
  <Layout.Column className="weather" justifyContent="center" alignItems="center">
    { props.children }
  </Layout.Column>
)

export const WeatherCardLower = (props: React.Props<any>) => (
  <section className="content">
    { props.children }
  </section>
)

export default WeatherCard
