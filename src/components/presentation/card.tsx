import * as React from 'react'
import * as Layout from '@components/layout'

import '@styles/card'

export interface ICardProps {
  title?: string | JSX.Element
}

const CardTitle = (props: React.Props<any>) => {
  if (!props.children) return null
  return(
    <span className="title">
      { props.children }
    </span>
  )
}

const Card = (props: React.Props<any> & ICardProps) => (
  <section className="card">
    <CardTitle>
      { props.title }
    </CardTitle>
    <Layout.Row className="content">
      { props.children }
    </Layout.Row>
  </section>
)

export default Card
