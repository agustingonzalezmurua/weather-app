import * as React from 'react'

import '@styles/button'

const Button = (props: React.ButtonHTMLAttributes<HTMLButtonElement>) => (
  <button disabled={props.disabled} className="button" onClick={props.onClick}>
    { props.children }
  </button>
)

export default Button
