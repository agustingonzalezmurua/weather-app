import * as React from 'react'

export interface IIconProps {
  icon: string
}
const Icon = (props: React.Props<any> & IIconProps) => (
  <i className={"wi wi-owm-" + props.icon} />
)

export default Icon
