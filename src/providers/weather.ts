/**
 * You need to be a registered user and provide your API key in order to use the weather API, more info: https://openweathermap.org/api
 */
const API_KEY = 'cd47a300245724cd93702975c1107e8e'
const API_URL = 'http://api.openweathermap.org/data/2.5/weather?units=metric'

export default {
  byGeo: (position: Position) => (
    fetch(`${API_URL}&lat=${position.coords.latitude}&lon=${position.coords.longitude}&APPID=${API_KEY}`)
      .then(response => response.json())
  )
}
