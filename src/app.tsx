import * as React from 'react'
import * as ReactDOM from 'react-dom'
import * as Layout from '@components/layout'

import '@styles/app'

import Weather from '@components/weather'

const App = () => (
  <Layout.Column className="fill-height" justifyContent="center" alignItems="center">
    <Weather/>
  </Layout.Column>
)

ReactDOM.render(<App/>, document.getElementById('app'))
