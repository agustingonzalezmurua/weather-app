const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

module.exports = (env, argv) => {
  const devMode = argv.mode !== 'production'

  devMode ? console.log('👨‍💻 DevMode engaged 👨‍💻') : console.log('🔥 Production Time 🔥')

  return ({
    plugins: [
      new MiniCssExtractPlugin({
        filename: devMode ? '[name].css' : '[name].[hash].css',
        chunkFilename: devMode ? '[id].css' : '[id].[hash].css',
      }),
      new HtmlWebpackPlugin({
        hash: true, 
        title: 'Weather App!',
        template: './src/index.html'
      })
    ],

    entry: './src/app.tsx',
    output: {
      path: path.resolve(__dirname, "dist"),
      filename: 'bundle.js',
    },

    resolve: {
      alias: {
        '@styles': path.resolve(__dirname, 'src/assets/styles/'),
        '@components': path.resolve(__dirname, 'src/components/'),
        '@providers': path.resolve(__dirname, 'src/providers/')
      },
      extensions: ['.tsx', '.ts', '.scss', '.js', '.jsx', '.css']
    },
  
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          loader: 'awesome-typescript-loader'
        },
        { 
          test: /\.s?[ac]ss$/,
          use: [
            devMode ? 'style-loader?hmr' : MiniCssExtractPlugin.loader,
            'css-loader',
            'sass-loader',
          ],
        },
        {
          test: /\.(eot|svg|ttf|woff|woff2)$/,
          loaders: ['file-loader?name=fonts/[name].[ext]'],
        }
      ]
    },

    devtool: devMode ? 'source-map' : undefined,

    devServer: {
      open: true,
    }
  })
}
